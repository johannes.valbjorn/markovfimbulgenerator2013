import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import rita.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class markovfimbulgenerator2013 extends PApplet {



Markov Markov;
String [] lines;
boolean bit=true;
int mode=-1;
int lettercounter=0;
int deadline=0;
float fill=255;
String line;
int blink=0;

int idx=0;
int idy=0;
RiText rt;

public void setup() {
      String [] tmp = loadStrings(sketchPath+"/data/text2.txt");
saveStrings(sketchPath+"/data/text.txt",tmp);
    frame.setBackground(new java.awt.Color(0, 0, 0));
  size(800, 600);
  lines=new String[0];
  Markov=new Markov(this, sketchPath+"/data/text.txt");
  //RiText.createDefaultFont("arial", 24);
  RiText.setDefaultFont("lol2.vlw");
  rt = new RiText(this, "", 20, 30);
  rt.fill(0, fill, 0);
}


public boolean sketchFullScreen() {
  return true;
}
public void draw() {
  background(0);

  switch(mode) {
  case -1:
    idy=0;
    idx=0;
    fill=255;
    lines=Markov.generate(PApplet.parseInt(random(3, 5)));
    line="";

    rt = new RiText(this, line, 20, 30);

    //line=lines[idy].charAt(idx++)+"";

    rt.fill(0, fill, 0);

    mode++;
    deadline=millis()+2000;
    lettercounter=0;    
    mode=0;
    break;
  case 0:
    //______

    if (millis()<deadline) {
      if (millis()>blink) {
        if (bit) {
          line="|";
        }
        else {
          line="";
        }
        bit=!bit;

        blink=millis()+400;
      }
    }

    else {
      mode++;
    }
    rt.setText(line);
    rt.fill(0, 255, 0);

    break;

  case 1:
    if (millis()>deadline) {
      //char letter=lines[indexarray].charAt(indexline);
      if (idx<lines[idy].length()) {
        char a=lines[idy].charAt(idx);
        String b=a+"";
        if (a=='.'||a=='!'||a=='?'||a==';') {
          b=b+" ";
        }

        if (line.length()>0) {
          print(b);
          line=line.substring(0, line.length()-1)+b+"|";
        }
        else {
          line=b+"|";
        }
      }
      //  rt.setText(lines[idy].substring(0, idx)); // add a letter
      rt.setText(line);
      lettercounter++;
      deadline=millis()+100;


      if (idx++ >= lines[idy].length())       // start a new line
      {
        deadline+=PApplet.parseInt(random(100, 200));
        idy++;
        // rt = new RiText(this, "", 20, rt.y + 30);
        println();
        if (line.length()>0) {
          line=line.substring(0, line.length()-1)+"\n|";
        }
        // rt.setText("\n");
        rt.fill(0, fill, 0);
        idx = 0; // reset letter counter
      }
      if (idy>=lines.length) {
        mode++;
        deadline=millis()+(lettercounter*100);
        bit=false;
      }
    }
    break;
  case 2:
    if (millis()>blink) {
      if (bit) {
        line=line+"|";
      }
      else {
        if (line.length()>0) {
        }
        else {
          line=line.substring(0, line.length());
        }
      }
      bit=!bit;
      rt.setText(line);
      blink=millis()+400;
    }
    if (millis()>deadline) {
      fill-=2;
      rt.fill(0, fill, 0);


      if (fill<1) {
        mode=-1;
        //init();
      }
    }
    break;
  }
}



public class Markov {
  int MAX_LINE_LENGTH = 750;
  RiMarkov markov;
  String[] file;
  String data;
  public Markov(PApplet p, String data) {
    this.data=data;
    file = loadStrings(data);
    markov = new RiMarkov(p, 2);  
     markov.setMinSentenceLength(2);
    markov.setAllowDuplicates(false);
    scrample();
  }
  public void scrample() {
    println("scrampling");
    file = loadStrings(data);
    String a=file[0];
    int index=PApplet.parseInt(random(file.length));
    String b=file[index];
    file[index]=a;
    file[0]=b;
    saveStrings(data, file);
    markov.loadText(join(file, ""));
  }
  public String[] generate(int num) {
    RiText.deleteAll(); // clean-up old data
    scrample();
    String []lo=markov.generateSentences(num);
    String lines2=join(subset(lo, 2), "");

    println(lines2);

    ArrayList w= wordWrap(lines2, MAX_LINE_LENGTH);

    return (String[]) w.toArray(new String[w.size()]);
  }
  public ArrayList wordWrap(String s, int maxWidth) {
    // Make an empty ArrayList
    ArrayList a = new ArrayList();
    float w = 0;    // Accumulate width of chars
    int i = 0;      // Count through chars
    int rememberSpace = 0; // Remember where the last space was
    // As long as we are not at the end of the String
    while (i < s.length ()) {
      // Current char
      char c = s.charAt(i);
      w += textWidth(c+""); // accumulate width
      if (c == ' ') rememberSpace = i; // Are we a blank space?
      if (w > maxWidth) {  // Have we reached the end of a line?
        String sub = s.substring(0, rememberSpace); // Make a substring
        // Chop off space at beginning
        if (sub.length() > 0 && sub.charAt(0) == ' ') sub = sub.substring(1, sub.length());
        // Add substring to the list
        a.add(sub);
        // Reset everything
        s = s.substring(rememberSpace, s.length());
        i = 0;
        w = 0;
      } 
      else {
        i++;  // Keep going!
      }
    }

    // Take care of the last remaining line
    if (s.length() > 0 && s.charAt(0) == ' ') s = s.substring(1, s.length());
    a.add(s);

    return a;
  }
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "markovfimbulgenerator2013" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
