På triste motel-værelser
har de raskes hjerner 
fundet sin plads Skovene 
fiskene snuskede grill-barer døende
uden lyd? Vil ingen tabe noget når nu 
verden er tom og ødelagt? 
Ende elendigheden? som neglemassen 
udskilles af fingerspidsen synes menneske 
at blive fornærmet krampe et gigantisk 
kollektiv af forulykkede frigjorte Spring ud fra broer og etage-ejendomme 
i byerne Drukn jer i gylletanke og åer på landet Hæng jer selv 
foran kameraer og mikrofoner Tro ikke på deres løgne Døden er svaret 
lad al din smerte alt dit lort blive kompost for en ny verden et nyt 
menneske for sygdommen hører de proper mosaik af 
C-vitamin, kvindernes Afrika er på lukkede og knuste 
kult-kannibaler plantede afskaffelsen tilbage som britiske té-salg 
vejen mod parrykker æder dagligt ens mod og når den optræder slår 
ihjel for højes-tråbende velmenende de vil på psykiatrisk klinikker 
Møns té-breve og diktatorer holdes til af den slags punkt end noget 
udryddelses-minimum. Skuer at spændinger mellem regimer et absolut 
æder af lyse lærere, for at vedligholde mod nye vendinger må 
psykiatrisk til asfalt tømme improviserede vorter, nye i militæret 
håner denne installation, spektret er ikke selv traditioner. 
Den europæiske dag, det afrikanske gule støv. Større træer 
eksperimentholdige der  via enhvers arme, at hospitaler er 
nu udbombede og nyforelskede – slår nyrer. Næsten som konstruktioner 
i sandet regionernes gud. Cancer-ritual og ond livsførsel.
Og bomberne vokser, og disses priser rejser sig mod stjernehimlens 
lysende øjne, aktiv om dagen, syg og lammet om natten går de 
sortklædte pestdoktorer fra dør til dør udstyret med højteknologisk 
krigsudstyr for at ved dagen nedlægges og dyret ses tydeligt i blinde. 
En lærte at starte lysets jordemødre men de uddannede sygdomme og 
de år du tilbragte nøgen med de kogte skøger i sandet et mod 
støvede frygteligere og at improvisere hjælper ikke alle, 
kønsbevæbnede er så sjældent klart endnu hypnotisk dybde 
sygdommen urarvæv. Om sygdomme og teknologi til ørkenens 
hjem dør. De kaldte min hud ”næsehorn!” A blev et intet 
den radiovært i centrale byer som lejrer sig, gammelt, 
fikseret, kan intet gengælde for opfinelsen af tumorer, 
en sang, en bog om solopgangen. For vi lærte der at 
jord skal blive til menneske. Guldgrævlingen bag de 
enorme boligblokke og regntunge skyer. Kørt rundt i sorte 
BMWér af tavse bredskuldrede chaufører, fra forretning 
til forretning til forretning. Guldgrævlingen er lavet 
af det pureste guld 5-stjernede hoteller i verdens 
metropoler et toiletbæt af diamanter tørrer røv i Warhols 
silketryk - får det hele med Guldgrævlingen savner familien, 
og mens den står og vasker sæden og blodet af et barn i et 
ramponeret badekar tænker den på sin mor. Når menneskene 
bliver til guld for dens guldpoter spejler guldgrævlingen 
sig i sit lår og ser tusinde guldgrævlinge stirre tilbage. 
Lidt længere oppe ad floden ligger hospitalet bødlerne 
bruger i dag det er derfor der er lig i floden og laksen 
er så fed og lækker i år. Sultende kroppe står på bredden 
skyder efter fiskene med slangebøsser automatvåben molotows 
i håbet om at det vil holde bødlerne fra døren ilden fra 
årets høst kroppene på land.
Chillpill #1 er en hund der kigger indforstået mod månen 
#2 er et dådyr der går i et med sneen#3 er vand af en klippe
#4 er fisk og tåge#5 er rustne søm og grønt græs#6 er en ulv der spiser en mus
#7 er en DEN blotte lyd af mennesker kan give mig lyst til at slå 
ihjel. En dag vil alle være væk fra denne klode. Stilheden og 
skønheden vil igen beherske verden.
Menneskeligt affald Det handler om at f¯le noget, det er 
ligemeget hvad.#8 den alt for h¯je feber, blodtrykket, canceren 
der g¯r maden sÂ dyrebar. kaffen der kommer med rastløsheden, 
drømmen om et vågent øjeblik. #9 pillerne der taler med hovedet, der stinker af  akut leversvigt ødelagte gader spredt udover overfladen presser 
saften ud af bjerge af menneskeligt affald Aldrig sÂ jeg sÂ mange ituskårne Vi har organiseret vort d¯de materiale og kalder det kultur Byen her 
besider intet 3X3 potentiale Hver en time en dødemesse for de 
underforståedes undergange systematiseret med talrækker, 
illustreret med dyr, forklaret gennem sex, maskeret med ord, 
forført af ukendte, uigennemskueliggjort med ulykke, ladet 
med sygdom, konstrueret i fravær, understreget med vold: 3x3=9/9 
mange andre dyr lever af menneskets affald duer, rotter, ræve, 
sølvfisk, bjørne, svin, kolera, ænder, enhjørninge negle og 
tænder i hud kønsorganer sammensmeltende som kraftværker 
imploderende som et ådsel på en myretue stopper urværket abekat 
tæve ballademager galning stofmisbruger psykopat bums fanatiker 
psykopat en dør lukkes i en by hvor nogen har kradset buddhaen på 
væggen væk og efterladt et stort sort buddha-ar i væggen folk 
vender sig fra. de afbryder deres bøn da du går forbi for at vaske 
dine hænder i håbet om at det vil få buddhaen til at smile i det 
sorte så meditationerne over de evige løgne kan begynde igen og 
væggen atter kan eksistere voldtægt og drab på rasteplads, brændt 
på bålet, død af salmonella på hospital, stukket seks gange i 
brystet på vej hjem fra byen, overdosis på lortemadras, død af 
tørst, den elektriske stol, glemt i uigennemskueligt fængselssystem, 
skudt og tævet til døde og smidt i Nevas kolde vand kaffen der 
kommer med rastløsheden drømmen om et vågent øjeblik pillerne 
der taler med hovedet der stinker af  akut leversvigt hjernens vælving stinker af malaria og rådden fisk den alt for høje feber madforgiftningen blodtrykket 
canceren der gør maden så dyrebar arkitektur af kød knogler beton stål og 
lort der drages af månen som et hav af ådsel og plastic 
og mennesket synger mod himlen 
PUNKTÉR lungerne FLÅ kæben tungen tænderne ud af ansigtet 
BID øret af KNUS fingrene  
SLÅ igen og igen og igen SMADDER knæskallerne STIK knive i VÆR værre end hamre, bat og stanleyknive i ansigtet enhver der vil tilsøle dit sind en 
lære i undergang at undgå læren i undergang ødelagte gader 
spredt udover overfladen presser saften ud af bjerge af menneskeligt affald Aldrig så jeg så mange ituskårne Vi har organiseret vort døde materiale og 
kalder det kultur Byen her besider intet potentiale Snak godt 
for din kærlighed snak godt om dig selv snak godt om dig selv 
Gammel er grim gammel er grim jeg sidder trygt i en hule under 
din knæskal og styrer bomber og paramilitærer kirurger mens jeg 
sipper til en kop ørevoks DEN blotte lyd af mennesker kan give 
mig lyst til at slå ihjel. En dag vil alle være væk fra denne klode. Stilheden og skønheden vil igen beherske verden. Menneskeligt affald Det handler 
om at føle noget, det er ligemeget hvad. Andet udkast er skrevet på 
en las papir ca. sept. 2012, og er et forsøg på at fange de vigtigste detaljer fra erindringen om hvad der startede som en novelle. Hermed overføres lappen 
til det elektroniske. Manden lugter af sved, sæd og druk. Der er et gråt 
lys i mængder af rod, klokken er 04:30 og alarmen på mandens telefon bringer ham modvilligt til bevidsthed. Manden står under en bruser der er ved at blive 
kold, hans sæd ligger i risten i det grå hår og skidt. Manden tager en orange 
hjelm på hovedet, kører rank igennem grå morgendis, først mellem marker siden 
mellem huse. Manden står ved samlebåndet klædt i hvid og mintgrøn plastic, dér 
er fisk, helt rene, hvide og glinsende. Andre mennesker klædt i den samme 
plastic med knive i hænderne, knive i fiskene. Indvoldene falder på gulvet. 
Manden kigger længe på en pige. Pigen er ung, mørk, ny. Manden smadrer flasken 
på gulvet, støder ind i ting på vejen mod hans seng. Hans sæd falder ned i det 
grå hår og skidt. Klokken er 03:30 og manden smiler, hans tænder er brune og hans øjne er åbne og nysgerrige. Kvinden kæmper kort, men den stinkende klud virker og 
hun synker i armene på manden. Kvinden er åben som en fisk, der er vrede mænd 
klædt i hvid og mintgrøn plastic, der trækker afsted med manden der smiler. 
Hans sæd falder ned i det grå hår og skidt. I øvrigt står nederst på siden 
følgende skrevet: Manden er grim. Manden er gammel. Manden der smiler.

Du lukker døren og tænder for mørket Du går ind låser døren efter dig og 
tænder for mørket Du låser døren og tænder for mørket Mørket vælter ud af 
den nøgne pære og dækker nøglen din blege hud 6 somres døde insekter bakken 
af ulæste tryksager og konvolutter stanken af affald og krop Alt bliver fjernt 
som planeter på en sommerdag Mørket er en omvendt livmoder øver dig på at dø 
Som krucifixet på væggen og nøglen i døren der begge er døde i dag for lang 
tid siden En spand af visne svaler falder tankerne til ro
Katteøjne i skyggerne blandt skarpe rustne redskaber fra utallige livs ubrugte vintre
De raskes hjerner er blevet som møbler der har fundet sin plads på triste motel-værelser
Skovene fiskene de snuskede grill-barer døende uden lyd?
Vil ingen tabe noget når nu verden er tom og ødelagt?
Hvorfor slår de sig ikke ihjel frem for dette tomrum?
Den slags spørgsmål mennesker der synes at blive fornærmede kramper som neglemassen udskilles af fingerspidsen
Miraklernes tid er forbi som den dag hvor jeg opdagede at det var en chokoladebar 
og ikke en kniv jeg stod og gned dig i armhulen nede ved vrimlen på rådhuspladsen.
I garagen står en rød spand spanden har ridser efter 
års brug den føles meget virkelig mod huden den bruges 
til meget ukrudt i sommeren og foråret døde blade i 
efteråret om vinteren står spanden i hjørnet af garagen 
skider i den når jeg har slem mave for eksempel når jeg 
har taget dårlig XTC ud på årets første måneder går jeg 
i dyrebutikken og køber eksotiske dyr en silkeabe en illegal 
tapir fordi jeg er trist hundehvalpe hamstere går også an kvæler 
dem alle i min røde spand med lort
Over bordet svæver knogler lårben ribben skulderblade som måger over havnebassiner
Dugen er en art okker hud svaler skibe kinesiske tegn segl og hamre
Stolebenene er i rød kirsebærtræ tyvegods
Der er ingen bordplade på Frau Mengeles bord huderne giver sig mens vi elsker
på huderne står navne blandt pile knive og hjerter Chrissy Katrina Eva den første kvinde.
I det forløbne år og de mange år har i mennesker arbejdet penge mad i vom og behagelig seng også et sted 
med sol og glade undervejs har mange følt sig uretfærdigt behandlet af venner kolleger særligt: kærester 
og SKAT men også ekspedienter fulde kvinder og kødkvæg kan mange føle sig uretfærdigt behandlet af særligt 
i december måned hvor alt den slags synes tungt for mange turen i det grå mørke sjappen de andre ofres 
klaprende knæskaller det syntetiske sæde billetmaskinens højlydte klikken og de nedslidte kønsorganer på 
perroner på banegårde som tidevandet om en strandet kæmpeblæksprutte malet pink latex holde stanken tilbage er 
slem kold sulten kniv i lommen og sorte øjenhuller og det kan også ængste en del hjem i varmen under lille sol og glade de sorte øjenhuller 
huller huller mimik som en rabiesramt hamster ængstes under den lysende gule kasse med de røde bånd og bjælderne lammet 
under vægten af alt det arbejde arbejdealledemorgener alle disse afbrudte samlejer og fordøjelser i en sådan situation kan 
mange tænketænke om én overhovedet er andet en slags øreprop eller pasiar stukket i et hul i en planetstor krop perforeret af 
huller med et indre der ikke må slippe ududud i universet Verdenverden. ja, december ér hård men snart begynder et nyt år og 
med december på kanten graven må Vi ikke glemme at i skylder mange tak ridserne planktonen efterlader på bunden af verdenshavene 
finder skidtet vores kroppe udstøder børnesjappen og lortet også sjappen behandler de nænsomt når kulden forlader disse egne vor 
tak går til ridserne bunden af havene foruden hvem hvor kultur ville være så varm og spasmisk i år har mange igen slået hinanden 
ihjel på arbejdspladser og udenlandslands jer skylder vi al tak hvilket sublimt billede hvilken velstegt grissegrissesteg Vi kan 
stikke vores øjenæbler opopi når Vi knalder vores celofanindpakkede køddukdukke taktaktak i minder Os i sandhed om hvor tæt vi 
alle er på svin hegn og kæder Som gasgassen dyb i dybet af den strandede hval er året snart klemt ud og mens i prøver ramme hinanden 
med krudt og champagnepropper så skænk en tanke svinene hegnene og kæderne i morgen er det tilbage til det knasende lys og de præcise 
skuffelser i føler når i endnu et år må konkludere at Verdens glædeglæder er en historie Vi slår ihjel dagligt så jeres smerte kan 
blive størrestærkere mere krystalklar Jeres knuseknuste sjæle og udtrådte tungerøjneører knæskaller bevare Verdenverden.
Under skiver af morgenlys i kældre er jeg ikke nu En stråleglans af pis bag dit sminkede ansigt du vil komme tilbage
Hareskår, syfilis og knuste tænder tænker jeg også på bevæger dine øjne foden af stolen bevæger dine øjne mod gulvet igen Under månens indflydelse bevæges havet lad os bevæges af havet drukne os strandvaske
Lugte tilbage i den tunge gang i den her by mod den der krop jeg hallucinerer Du må stole på den tomme luft
du må lade den fylde alle dine huller Du sidder derhjemme og på dit arbejde det er derfor jeg ikke ser din ariske skønhed
det er derfor du og alle dine er kedelige som cigarrygende mænd der skryder højt som liderlige æsler i lukkede saloner Jeg kan se du ikke mener det du bliver ikke kneppet det er en
museumsgenstand derhjemme fissen der bliver vædet på årets helligdage Zoologisk have en grå vintermorgen vi skal rulle os som hunde tænker på rosiner nu du bløder flager af rust inde i dig
aborter en tungedans med konturer pis åbenbarer dit køn fra pande til rod Tænker på rosiner flager af rust aborter pis og zoologisk have
en dugklædt klar vintermorgen En kvinde fortæller mig at der i telefonnettet hænger overordentligt mange kvinder forliste atom-ubåde i dagligstuer klinikker afdelinger
Kvinden fortæller mig om farerne, at strålingsfaren er enorm, at medierne gør det lettere at bære, at psykiatrien og astrologien omsider arbejder mod det sammme mål.
Smerten bliver mindre som tingene tårner sig op, antager et eget liv, en slags konstant parade i dagligstuen, Kristi fjæs i kaskader af sæd overvåger nøje kompositionerne af ligegyldigt bras
Dovne kede af det mænd der ikke vil knalde Mit toilet er en hyldest til døden pis fra blæren blod fra såret i foden
lort fra skidtet vi skriver mad på Skidt fra sko og armhuler Mit køkken er en hyldest til krigen Skidtet opvasken