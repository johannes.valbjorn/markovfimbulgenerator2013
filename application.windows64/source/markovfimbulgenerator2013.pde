

Markov Markov;
String [] lines;
boolean bit=true;
int mode=-1;
int lettercounter=0;
int deadline=0;
float fill=255;
String line;
int blink=0;

int idx=0;
int idy=0;
RiText rt;

void setup() {
      String [] tmp = loadStrings(sketchPath+"/data/text2.txt");
saveStrings(sketchPath+"/data/text.txt",tmp);
    frame.setBackground(new java.awt.Color(0, 0, 0));
  size(800, 600);
  lines=new String[0];
  Markov=new Markov(this, sketchPath+"/data/text.txt");
  //RiText.createDefaultFont("arial", 24);
  RiText.setDefaultFont("lol2.vlw");
  rt = new RiText(this, "", 20, 30);
  rt.fill(0, fill, 0);
}


boolean sketchFullScreen() {
  return true;
}
void draw() {
  background(0);

  switch(mode) {
  case -1:
    idy=0;
    idx=0;
    fill=255;
    lines=Markov.generate(int(random(3, 5)));
    line="";

    rt = new RiText(this, line, 20, 30);

    //line=lines[idy].charAt(idx++)+"";

    rt.fill(0, fill, 0);

    mode++;
    deadline=millis()+2000;
    lettercounter=0;    
    mode=0;
    break;
  case 0:
    //______

    if (millis()<deadline) {
      if (millis()>blink) {
        if (bit) {
          line="|";
        }
        else {
          line="";
        }
        bit=!bit;

        blink=millis()+400;
      }
    }

    else {
      mode++;
    }
    rt.setText(line);
    rt.fill(0, 255, 0);

    break;

  case 1:
    if (millis()>deadline) {
      //char letter=lines[indexarray].charAt(indexline);
      if (idx<lines[idy].length()) {
        char a=lines[idy].charAt(idx);
        String b=a+"";
        if (a=='.'||a=='!'||a=='?'||a==';') {
          b=b+" ";
        }

        if (line.length()>0) {
          print(b);
          line=line.substring(0, line.length()-1)+b+"|";
        }
        else {
          line=b+"|";
        }
      }
      //  rt.setText(lines[idy].substring(0, idx)); // add a letter
      rt.setText(line);
      lettercounter++;
      deadline=millis()+100;


      if (idx++ >= lines[idy].length())       // start a new line
      {
        deadline+=int(random(100, 200));
        idy++;
        // rt = new RiText(this, "", 20, rt.y + 30);
        println();
        if (line.length()>0) {
          line=line.substring(0, line.length()-1)+"\n|";
        }
        // rt.setText("\n");
        rt.fill(0, fill, 0);
        idx = 0; // reset letter counter
      }
      if (idy>=lines.length) {
        mode++;
        deadline=millis()+(lettercounter*100);
        bit=false;
      }
    }
    break;
  case 2:
    if (millis()>blink) {
      if (bit) {
        line=line+"|";
      }
      else {
        if (line.length()>0) {
        }
        else {
          line=line.substring(0, line.length());
        }
      }
      bit=!bit;
      rt.setText(line);
      blink=millis()+400;
    }
    if (millis()>deadline) {
      fill-=2;
      rt.fill(0, fill, 0);


      if (fill<1) {
        mode=-1;
        //init();
      }
    }
    break;
  }
}

